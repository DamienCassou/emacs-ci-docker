# Variables passed to the Dockerfile through --build-arg
ARG BASE

FROM $BASE

# Variables passed to the Dockerfile through --build-arg
ARG EMACS_VERSION
ARG EMACS_URL

RUN bash /root/fetch-and-build-emacs.sh $EMACS_VERSION $EMACS_URL

RUN rm -rf /var/lib/apt/lists/*
