#!/usr/bin/env bash

set -evx

export DEBIAN_FRONTEND=noninteractive

VERSION="$1"
DOWNLOAD_URL="$2"

PREFIX="/usr"

echo Building for Emacs version "$VERSION"

mkdir -p temp
cd temp

mkdir -p "$PREFIX"

CURL="curl -fsSkL --retry 9 --retry-delay 9"

$CURL "$DOWNLOAD_URL"/emacs-"$VERSION".tar.xz > emacs.tar.xz

tar xfJ emacs.tar.xz

cd emacs-"$VERSION"

./configure --prefix="$PREFIX" --with-x-toolkit=no --without-x --without-all --with-gnutls
make
make install

$PREFIX/bin/emacs --version
ldd $PREFIX/bin/emacs
